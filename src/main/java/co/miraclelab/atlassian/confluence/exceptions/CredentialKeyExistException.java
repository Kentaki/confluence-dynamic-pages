package co.miraclelab.atlassian.confluence.exceptions;

/**
 * Created by kentaki on 9/17/17.
 */
public class CredentialKeyExistException extends Exception{
    public CredentialKeyExistException(){};
    public CredentialKeyExistException(String m){
        super(m);
    }
}
