package co.miraclelab.atlassian.confluence.servlet;

import co.miraclelab.atlassian.confluence.ao.SavedCredential;
import co.miraclelab.atlassian.confluence.ao.SavedCredentialService;
import co.miraclelab.atlassian.confluence.exceptions.CredentialKeyExistException;
import co.miraclelab.atlassian.confluence.model.Credential;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.core.util.StringUtils;
import com.atlassian.elasticsearch.shaded.netty.util.internal.StringUtil;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.component.ComponentLocator;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.atlassian.upm.api.license.entity.PluginLicense;
import com.atlassian.user.EntityException;
import com.atlassian.user.Group;
import com.atlassian.user.GroupManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.net.URI;
import java.util.*;

@Named("JSMacroConfigServlet")
public class JSMacroConfigServlet extends HttpServlet{
    private static final Logger log = LoggerFactory.getLogger(JSMacroConfigServlet.class);
    @ComponentImport
    private final UserManager userManager;
    @ComponentImport
    private final LoginUriProvider loginUriProvider;
    @ComponentImport
    private final TemplateRenderer templateRenderer;
    @ComponentImport
    private final ActiveObjects ao;
    @ComponentImport
    private final PluginLicenseManager licenseManager;

    private final SavedCredentialService savedCredentialService;
    private final UserAccessor userAccessor;
    private final GroupManager groupManager;

    private String baseUrl;

    private final static String SUCCESS="success";
    private final static String FAIL="error";
    private final static String SEPERATOR=", ";

    @Inject
    public JSMacroConfigServlet(UserManager userManager, LoginUriProvider loginUriProvider,ActiveObjects ao,
                             TemplateRenderer templateRenderer,PluginLicenseManager licenseManager) {
        this.userManager = userManager;
        this.loginUriProvider = loginUriProvider;
        this.templateRenderer = templateRenderer;
        this.licenseManager= licenseManager;
        userAccessor = ComponentLocator.getComponent(UserAccessor.class);
        groupManager = ComponentLocator.getComponent(GroupManager.class);
        baseUrl = ComponentLocator.getComponent(SettingsManager.class).getGlobalSettings().getBaseUrl();
        this.ao=ao;
        savedCredentialService=new SavedCredentialService(ao);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String licenseCheck=request.getParameter("licenseCheck");
        if(licenseCheck!=null && licenseCheck.equals("true")){
            if(checkLicense())
                response.getWriter().write("success");
            else response.getWriter().write("error");
            return;
        }

        Map<String, Object> context = new HashMap<String, Object>();
        context.put("baseURL", baseUrl);

        UserProfile userProfile = userManager.getRemoteUser(request);
        if (userProfile == null) {
            redirectToLogin(request, response);
            return;
        }

        if (!isAdmin(getCurrentUser(request))) {
            response.sendRedirect(baseUrl);
            return;
        }

        List<Credential> credentials= new ArrayList<Credential>();
        for(SavedCredential savedCredential : savedCredentialService.all())
            credentials.add(new Credential(savedCredential));
        context.put("credentials", credentials);

        List<String > groups=getGroups();
        context.put("groups",groups);

        templateRenderer.render("templates/view.vm", context, response.getWriter());
    }

    private List<String> getGroups() {
        List<String> groups= new ArrayList<String>();
        try {
            Iterator<Group> groupIterator=groupManager.getGroups().iterator();
            while (groupIterator.hasNext()){
                groups.add(groupIterator.next().getName());
            }
            return groups;
        } catch (EntityException e) {
            log.error(e.getMessage(),e);
            return groups;
        }
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
        Writer out =response.getWriter();
        if(!regularCheck(request,response))
            return;

        String idString= request.getParameter("id");
        if(idString==null || idString.equals("") || !org.apache.commons.lang3.StringUtils.isNumeric(idString)){
            out.write(FAIL+SEPERATOR+"Can't find such credential!");
            return;
        }

        savedCredentialService.toggleMe(idString);
        out.write(SUCCESS);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
        Writer out =response.getWriter();
        if(!regularCheck(request,response))
            return;


        String idString= request.getParameter("id");
        if(idString==null || idString.equals("") || !org.apache.commons.lang3.StringUtils.isNumeric(idString)){
            out.write(FAIL+SEPERATOR+"Can't find such credential!");
            return;
        }

        savedCredentialService.deleteMe(idString);
        out.write(SUCCESS);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Writer out =response.getWriter();
        if(!regularCheck(request,response))
            return;


        String key = request.getParameter("key");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String[] access = request.getParameterValues("access");

        if(key==null || key.equals(""))
            out.write(FAIL+SEPERATOR+"Credential was not valid to save!");
        key=key.toLowerCase();

        Credential credential = new Credential();
        credential.setActive(false);
        credential.setUsername(username);
        credential.setPassword(password);
        credential.setKey(key);

        if(access!=null && access.length>0)
            credential.setAccess(access);


        if(credential.isValid()){
            try {
                savedCredentialService.add(credential);
                out.write(SUCCESS);

            } catch (CredentialKeyExistException e) {
                log.error(e.getMessage(),e);
                out.write(FAIL+SEPERATOR+e.getMessage());
                return;
            }
        }else out.write(FAIL+SEPERATOR+"Credential was not valid to save!");

    }

    private boolean regularCheck(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Writer out =response.getWriter();
        UserProfile userProfile = userManager.getRemoteUser(request);
        if (userProfile == null) {
            out.write(FAIL+SEPERATOR+"Your session has been expired!");
            return false;
        }

        if (!isAdmin(getCurrentUser(request))) {
            out.write(FAIL+SEPERATOR+"Admin privileges required!");
            return false;
        }
        return true;
    }

    private boolean isAdmin(ConfluenceUser user) {
        return ComponentLocator.getComponent(PermissionManager.class).isSystemAdministrator(user);

    }

    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }

    private URI getUri(HttpServletRequest request) {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null) {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }

    private ConfluenceUser getCurrentUser(HttpServletRequest req) {
        return userAccessor.getUserByKey(userManager.getRemoteUser(req).getUserKey());
    }

    private boolean checkLicense(){
        if (licenseManager.getLicense().isDefined()) {
            PluginLicense pluginLicense = licenseManager.getLicense().get();
            if (pluginLicense.getError().isDefined())
                return false;
            else
                return true;
        }else
            return false;
    }

}