package co.miraclelab.atlassian.confluence.ao;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.StringLength;

/**
 * Created by kentaki on 9/17/17.
 */

@Preload
public interface SavedCredential extends Entity {
    String getUsername();
    String getPassword();
    String getKey();
    @StringLength(value=StringLength.UNLIMITED)
    String getAccess();
    boolean isActive();

    void setUsername(String username);
    void setPassword(String password);
    void setKey(String key);
    void setAccess(String access);
    void setActive(boolean active);

}
