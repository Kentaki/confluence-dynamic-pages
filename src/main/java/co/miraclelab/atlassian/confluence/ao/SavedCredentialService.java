package co.miraclelab.atlassian.confluence.ao;

import co.miraclelab.atlassian.confluence.exceptions.CredentialKeyExistException;
import co.miraclelab.atlassian.confluence.model.Credential;
import com.atlassian.activeobjects.external.ActiveObjects;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;

/**
 * Created by kentaki on 9/17/17.
 */
public class SavedCredentialService {
    private final ActiveObjects ao;

    public SavedCredentialService(ActiveObjects ao) {
        this.ao = checkNotNull(ao);
    }

    public SavedCredential add(String username, String password, String key, String access, boolean active) throws CredentialKeyExistException {
        if(this.findCredentialbyKey(key.toLowerCase())!=null)
            throw new CredentialKeyExistException("Key "+key+" Already Exist!");
        final SavedCredential credential = ao.create(SavedCredential.class);
        credential.setUsername(username);
        credential.setPassword(password);
        credential.setKey(key.toLowerCase());
        credential.setAccess(access);
        credential.setActive(active);
        credential.save();
        return credential;
    }

    public SavedCredential add(Credential c) throws CredentialKeyExistException {
        if(this.findCredentialbyKey(c.getKey().toLowerCase())!=null)
            throw new CredentialKeyExistException("Key "+c.getKey()+" Already Exist!");
        final SavedCredential credential = ao.create(SavedCredential.class);
        credential.setUsername(c.getUsername());
        credential.setPassword(c.getPassword());
        credential.setKey(c.getKey().toLowerCase());
        credential.setAccess(c.parseAccess());
        credential.setActive(c.isActive());
        credential.save();
        return credential;
    }

    public List<SavedCredential> all() {
        return newArrayList(ao.find(SavedCredential.class));
    }

    public List<SavedCredential> allActive() {
        return newArrayList(ao.find(SavedCredential.class, "ACTIVE = ?",true));
    }

    public List<SavedCredential> allActiveForGroup(String group) {
        return newArrayList(ao.find(SavedCredential.class, "ACCESS LIKE ? AND ACTIVE = ?", group,true));
    }

    public List<SavedCredential> allforGroup(String group) {
        return newArrayList(ao.find(SavedCredential.class, "ACCESS LIKE ?", group));
    }

    public void deleteMe(String id) {
        for (SavedCredential credential : ao.find(SavedCredential.class, "ID = ?", Integer.parseInt(id))) {
            ao.delete(credential);
        }
    }

    public void toggleMe(String id) {
        for (SavedCredential credential : ao.find(SavedCredential.class, "ID = ?", Integer.parseInt(id))) {
            if(credential.isActive())
                credential.setActive(false);
            else
                credential.setActive(true);
            credential.save();
        }
    }

    public SavedCredential findCredential(String id){
        SavedCredential result=null;
        for (SavedCredential credential : ao.find(SavedCredential.class, "ID = ?", Integer.parseInt(id))) {
            result=credential;
        }
        return result;
    }

    public SavedCredential findCredentialbyKey(String key){
        SavedCredential result=null;
        for (SavedCredential credential : ao.find(SavedCredential.class, "KEY = ?", key)) {
            result=credential;
        }
        return result;
    }

    public int count(){
        return ao.count(SavedCredential.class);
    }
}
