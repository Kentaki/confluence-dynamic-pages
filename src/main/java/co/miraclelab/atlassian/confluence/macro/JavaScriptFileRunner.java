package co.miraclelab.atlassian.confluence.macro;

import co.miraclelab.atlassian.confluence.ao.SavedCredential;
import co.miraclelab.atlassian.confluence.ao.SavedCredentialService;
import co.miraclelab.atlassian.confluence.model.Credential;
import co.miraclelab.atlassian.confluence.model.Scripter;
import co.miraclelab.atlassian.confluence.model.script.AttachmentScript;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.core.ContentEntityManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.script.ScriptException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kentaki on 9/17/17.
 */
@Named("JavaScriptFileRunner")
public class JavaScriptFileRunner implements Macro {
    private static final Logger log = LoggerFactory.getLogger(JavaScriptFileRunner.class);
    @ComponentImport
    private final AttachmentManager attachmentManager;
    @ComponentImport
    private final ActiveObjects ao;
    @ComponentImport
    private final ContentEntityManager contentEntityManager;

    private final SavedCredentialService savedCredentialService;
    private final Scripter scripter;
    @Inject
    public JavaScriptFileRunner(AttachmentManager attachmentManager, ActiveObjects ao, ContentEntityManager contentEntityManager){
        this.attachmentManager=attachmentManager;
        this.contentEntityManager= contentEntityManager;
        scripter=new Scripter();
        this.ao=ao;
        savedCredentialService=new SavedCredentialService(ao);
    }
    public String execute(Map<String, String> map, String s, ConversionContext conversionContext) throws MacroExecutionException {
        boolean outputCheck=Boolean.valueOf(map.get("PrintOutput"));
        String jsFile=map.get("JavaScriptFile");
        int thisVersion=conversionContext.getEntity().getVersion();
        Attachment attachment=attachmentManager.getAttachment(conversionContext.getEntity(),jsFile,thisVersion);

        if(attachment==null)
            return JavaScriptLineRunner.error("Null Error!","Script file can't be null or empty!");

        if(!attachment.getMediaType().equals("text/javascript"))
            return JavaScriptLineRunner.error("Attachment Error!","Attachment is not a JavaScript file");

        HashMap<String,Object> objectHashMap;
        Object o=conversionContext.getProperty(JavaScriptLineRunner.OBJECT_MAP_KEY);
        if(o!=null && o instanceof HashMap)
            objectHashMap= (HashMap<String, Object>) o;
        else objectHashMap= new HashMap<String,Object>();

        AttachmentScript attachmentScript= new AttachmentScript(attachmentManager,
                conversionContext.getEntity(),contentEntityManager);

        try {
                List<Credential> credentialList = giveMeCredentials();
                scripter.setCredentialList(credentialList);
                scripter.setAttachmentScript(attachmentScript);
                scripter.objectMap=objectHashMap;
                Reader targetReader = new InputStreamReader(attachmentManager.getAttachmentData(attachment));
                String result=scripter.run(targetReader);
                conversionContext.setProperty(JavaScriptLineRunner.OBJECT_MAP_KEY,scripter.objectMap);
                if(outputCheck) return result;
                else return "";

            } catch (ScriptException e) {
                log.error(e.getMessage(),e);
                return JavaScriptLineRunner.error("JS Error!",e.getMessage());
            }

    }

    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    public OutputType getOutputType() {
        return OutputType.INLINE;
    }

    private List<Credential> giveMeCredentials(){
        List<Credential> credentialList = new ArrayList<Credential>();
        for(SavedCredential savedCredential : savedCredentialService.allActive())
            credentialList.add(new Credential(savedCredential));
        return credentialList;
    }
}
