package co.miraclelab.atlassian.confluence.macro;

import co.miraclelab.atlassian.confluence.ao.SavedCredential;
import co.miraclelab.atlassian.confluence.ao.SavedCredentialService;
import co.miraclelab.atlassian.confluence.model.Credential;
import co.miraclelab.atlassian.confluence.model.Scripter;
import co.miraclelab.atlassian.confluence.model.script.AttachmentScript;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.core.ContentEntityManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.script.ScriptException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kentaki on 9/17/17.
 */
@Named("JavaScriptLineRunner")
public class JavaScriptLineRunner implements Macro {
    public static final String OBJECT_MAP_KEY="dynamic-pages-object-map";
    private static final Logger log = LoggerFactory.getLogger(JavaScriptLineRunner.class);
    @ComponentImport
    private final AttachmentManager attachmentManager;
    @ComponentImport
    private final ActiveObjects ao;
    @ComponentImport
    private final ContentEntityManager contentEntityManager;

    private final SavedCredentialService savedCredentialService;
    private final Scripter scripter;
    @Inject
    public JavaScriptLineRunner(AttachmentManager attachmentManager, ActiveObjects ao,ContentEntityManager contentEntityManager){
        this.attachmentManager=attachmentManager;
        this.contentEntityManager=contentEntityManager;
        scripter=new Scripter();
        this.ao=ao;
        savedCredentialService=new SavedCredentialService(ao);
    }
    public String execute(Map<String, String> map, String inlineScript, ConversionContext conversionContext) throws MacroExecutionException {
        boolean outputCheck=Boolean.valueOf(map.get("PrintOutput"));

        if(inlineScript==null || inlineScript.equals(""))
            return error("Null Error!","Script can't be null!");
        List<Credential> credentialList = giveMeCredentials();

        HashMap<String,Object> objectHashMap;
        Object o=conversionContext.getProperty(OBJECT_MAP_KEY);
        if(o!=null && o instanceof HashMap)
            objectHashMap= (HashMap<String, Object>) o;
        else objectHashMap= new HashMap<String,Object>();

        AttachmentScript attachmentScript= new AttachmentScript(attachmentManager,
                conversionContext.getEntity(),contentEntityManager);

            try {
                scripter.setCredentialList(credentialList);
                scripter.setAttachmentScript(attachmentScript);
                scripter.objectMap=objectHashMap;
                String result=scripter.run(inlineScript);
                conversionContext.setProperty(OBJECT_MAP_KEY,scripter.objectMap);
                if(outputCheck) return result;
                else return "";
            } catch (ScriptException e) {
                log.error(e.getMessage(),e);
                return error("JS Error!",e.getMessage());
            }

    }

    public BodyType getBodyType() {
        return BodyType.PLAIN_TEXT;
    }

    public OutputType getOutputType() {
        return OutputType.INLINE;
    }

    private List<Credential> giveMeCredentials(){
        List<Credential> credentialList = new ArrayList<Credential>();
        for(SavedCredential savedCredential : savedCredentialService.allActive())
            credentialList.add(new Credential(savedCredential));
        return credentialList;
    }

    public static String error(String title,String errorMessage){
        return "<span class=\"aui-lozenge aui-lozenge-error\" title=\""+errorMessage.replaceAll("\"","'")+"\">"+title+"</span>";
    }
}
