package co.miraclelab.atlassian.confluence.model;

import co.miraclelab.atlassian.confluence.ao.SavedCredential;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by kentaki on 9/17/17.
 */
public class Credential {
    private int id;
    private String username;
    private String password;
    private String key;
    private boolean active;
    private static final String SEPARATOR =",";
    List<String> access= new ArrayList<String>();

    public Credential(){}
    public Credential(SavedCredential savedCredential){
        this.setId(savedCredential.getID());
        this.setActive(savedCredential.isActive());
        this.setPassword(savedCredential.getPassword());
        this.setUsername(savedCredential.getUsername());
        this.setAccess(exportGroups(savedCredential.getAccess()));
        this.setKey(savedCredential.getKey());
    }

    private List<String> exportGroups(String access) {
        List<String> groupList=new ArrayList<String>();
        if(access==null || access.equals(""))
            return groupList;
        String[] groups=access.split(SEPARATOR);
        for(String g : groups){
            if(g!=null && !g.equals("") && !groupList.contains(g))
                groupList.add(g);
        }
        return groupList;
    }

    public String parseAccess() {
        String result="";
        for(String g : access)
            result+=g+SEPARATOR;
        return result;
    }


    public void addGroupAccess(String group){
        if(!access.contains(group))
            access.add(group);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<String> getAccess() {
        return access;
    }

    public void setAccess(List<String> access) {
        this.access = access;
    }

    public void setAccess(String[] access) {
        this.access = Arrays.asList(access);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isValid() {
        if(this.username == null || this.username.equals("") || this.password == null || this.key==null || this.key.equals(""))
            return false;
        return true;
    }
}
