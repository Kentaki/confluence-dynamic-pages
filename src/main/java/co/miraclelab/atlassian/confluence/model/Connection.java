package co.miraclelab.atlassian.confluence.model;

import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by kentaki on 9/17/17.
 */
public class Connection {
    private String url;
    private String username;
    private String password;
    private int responseStatus;
    private String responseBody;
    private HttpClient httpClient;

    public Connection(){
        httpClient= HttpClients.createDefault();
    }

    public void doGet() throws AuthenticationException, IOException {
        HttpGet request = new HttpGet(url);
        if(username!=null && password!=null){
            BasicScheme scheme= new BasicScheme();
            request.addHeader(
                scheme.authenticate(new UsernamePasswordCredentials(username,password),request,new BasicHttpContext())
                 );
        }
        HttpResponse response=httpClient.execute(request);
        this.setResponseStatus(response.getStatusLine().getStatusCode());
        this.setResponseBody(EntityUtils.toString(response.getEntity()));
    }

    public void doPost(String json) throws AuthenticationException, IOException {
        HttpPost request = new HttpPost(url);
        if(username!=null && password!=null){
            BasicScheme scheme= new BasicScheme();
            request.addHeader(
                    scheme.authenticate(new UsernamePasswordCredentials(username,password),request,new BasicHttpContext())
            );
        }
        StringEntity params= new StringEntity(json);
        request.addHeader("content-type","application/json");
        request.setEntity(params);
        HttpResponse response = httpClient.execute(request);
        this.setResponseStatus(response.getStatusLine().getStatusCode());
        this.setResponseBody(EntityUtils.toString(response.getEntity()));
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(int responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    public HttpClient getHttpClient() {
        return httpClient;
    }

    public void setHttpClient(HttpClient httpClient) {
        this.httpClient = httpClient;
    }
}
