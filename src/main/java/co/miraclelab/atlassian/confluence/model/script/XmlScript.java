package co.miraclelab.atlassian.confluence.model.script;

import org.json.JSONObject;
import org.json.XML;

/**
 * Created by kentaki on 9/23/17.
 */
public class XmlScript {

    public String asJSON(String xml){
        JSONObject xmlJSONObj = XML.toJSONObject(xml);
        return xmlJSONObj.toString();
    }
}
