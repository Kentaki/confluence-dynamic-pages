package co.miraclelab.atlassian.confluence.model.script;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

public class HtmlScript {

    public String p(String p){
        return "<p>"+p+"</p>";
    }
    public String br(){
        return "<br/>";
    }

    public String element(String el){
        return element(el,null);
    }

    public String element(String el,String val){
        if(val==null) val="";
        return "<"+el+">"+val+"</"+el+">";
    }

    public String a(String ref){
        return a(ref,null,false);
    }
    public String a(String ref,String title){
        return a(ref,title,false);
    }
    public String a(String ref,String title, boolean t){
        String target="";
        if(ref==null || ref.equals(""))
            ref="#";
        if(title==null || title.equals(""))
            title=ref;
        if(t) target="target=\"_blank\"";
        return "<a title=\""+title+"\" href=\""+ref+"\" "+target+">"+title+"</a>";
    }

    public String table(String[] headers,String[][] rows){
        String table="<table>";
        table+="<thead><tr>";
        for(String th : headers)
            table+="<th>"+checkHtml(th)+"</th>";
        table+="</tr></thead><tbody>";
        for(String[] row : rows){
            table+="<tr>";
            for(String td : row)
                table+="<td>"+ checkHtml(td)+"</td>";
            table+="</tr>";
        }
        table+="</tbody></table>";
        return table;
    }

    public Elements find(String selector, String html){
        Document doc = Jsoup.parse(html);
        return doc.select(selector);
    }

    private String checkHtml(String html){
        Whitelist whitelist= Whitelist.basicWithImages();
        whitelist.addTags("h1", "h2", "h3", "h4", "h5", "h6");
        return Jsoup.clean(html,whitelist);
    }

}
