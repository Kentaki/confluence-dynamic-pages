package co.miraclelab.atlassian.confluence.model.script;

import com.atlassian.confluence.core.ContentEntityManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

/**
 * Created by kentaki on 9/25/17.
 */
public class AttachmentScript {
    private static final Logger log = LoggerFactory.getLogger(AttachmentScript.class);
    private final AttachmentManager attachmentManager;
    private final ContentEntityObject contentEntityObject;
    private final ContentEntityManager contentEntityManager;
    public AttachmentScript(AttachmentManager attachmentManager, ContentEntityObject contentEntityObject,
                            ContentEntityManager contentEntityManager){
        this.attachmentManager=attachmentManager;
        this.contentEntityObject=contentEntityObject;
        this.contentEntityManager=contentEntityManager;
    }

    public String read(String fileName){
        return read(contentEntityObject,fileName);
    }

//    public void write(String fileName,String content){
//        write(contentEntityObject,fileName,content);
//    }

    public Attachment get(String fileName){
        return get(contentEntityObject,fileName);
    }

    public String read(String fileName,int pageId){
        return read(getByPageId(pageId),fileName);
    }

//    public void write(String fileName,String content, int pageId){
//        write(getByPageId(pageId),fileName,content);
//    }

    public Attachment get(String fileName, int pageId){
        return get(getByPageId(pageId),fileName);
    }

    private ContentEntityObject getByPageId(int pageId){
        return contentEntityManager.getById(pageId);
    }

    private Attachment get(ContentEntityObject contentEntityObject,String fileName){
       return attachmentManager.getAttachment(contentEntityObject,fileName);
    }

    private String read(ContentEntityObject contentEntityObject,String fileName){
        Attachment attachment=attachmentManager.getAttachment(contentEntityObject,fileName);
        InputStream inputStream=attachmentManager.getAttachmentData(attachment);
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, StandardCharsets.UTF_8);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return writer.toString();
    }

//    private void write(ContentEntityObject contentEntityObject, String fileName,String content){
//        Attachment oldAttachment=attachmentManager.getAttachment(contentEntityObject,fileName); //TODo investigate version issue
//        Attachment newAttachment= new Attachment(oldAttachment.getFileName(),oldAttachment.getMediaType(),
//                oldAttachment.getFileSize(),"modified by JS macro");
//        InputStream inputStream = IOUtils.toInputStream(content, StandardCharsets.UTF_8);
//        try {
//            attachmentManager.saveAttachment(newAttachment,oldAttachment,inputStream);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
}
