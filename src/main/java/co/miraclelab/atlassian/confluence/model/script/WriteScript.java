package co.miraclelab.atlassian.confluence.model.script;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Writer;

/**
 * Created by kentaki on 9/23/17.
 */
public class WriteScript {
    private final Writer out;
    private final HtmlScript html;
    private static final Logger log = LoggerFactory.getLogger(HttpScript.class);

    public WriteScript(Writer out, HtmlScript html){
        this.out=out;
        this.html=html;
    }

    public void print(String s){
        try {
            out.write(s);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }
    public void println(String s){
        print(s);
        println();
    }

    public void println(){
        print(html.br());
    }

    public void p(String p){
        print(html.p(p));
    }


    public void element(String el){
        print(html.element(el,null));
    }

    public void element(String el,String val){
        print(html.element(el,val));
    }

    public void a(String ref){
        print(html.a(ref));
    }
    public void a(String ref,String title){
        print(html.a(ref,title));
    }
    public void a(String ref,String title, boolean t){
        print(html.a(ref,title,t));
    }

    public void table(String[] headers,String[][] rows){
        print(html.table(headers,rows));
    }
}
