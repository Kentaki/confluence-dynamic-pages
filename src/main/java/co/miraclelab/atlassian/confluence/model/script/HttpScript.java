package co.miraclelab.atlassian.confluence.model.script;

import co.miraclelab.atlassian.confluence.model.Connection;
import co.miraclelab.atlassian.confluence.model.Credential;
import org.apache.http.auth.AuthenticationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

/**
 * Created by kentaki on 9/17/17.
 */
public class HttpScript {
    private static final Logger log = LoggerFactory.getLogger(HttpScript.class);
    private final List<Credential> credentialList;

    public HttpScript(List<Credential> credentials){
        this.credentialList=credentials;
    }

    public String get(String url, String credentialKey){
        Credential credential= getMeCredential(credentialKey,credentialList);
        if(credential==null)
            return "";
        Connection connection = new Connection();
        connection.setUrl(url);
        connection.setUsername(credential.getUsername());
        connection.setPassword(credential.getPassword());


        return makeConnection(connection);
    }

    public String get(String url){
        Connection connection = new Connection();
        connection.setUrl(url);
        return makeConnection(connection);
    }

    private static Credential getMeCredential(String credentialKey, List<Credential> credentialList) {
        for(Credential c : credentialList)
            if(c.getKey().equalsIgnoreCase(credentialKey))
                return c;
        return null;
    }

    private static String makeConnection(Connection connection){
        try {
            connection.doGet();
            return connection.getResponseBody();

        } catch (AuthenticationException e) {
            log.error(e.getMessage(),e);
            return "";
        } catch (IOException e) {
            log.error(e.getMessage(),e);
            return "";
        }
    }


}
