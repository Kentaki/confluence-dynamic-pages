package co.miraclelab.atlassian.confluence.model;

import co.miraclelab.atlassian.confluence.model.script.*;

import javax.script.*;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kentaki on 9/17/17.
 */
public class Scripter {
    private List<Credential> credentialList;
    public Map<String,Object> objectMap= new HashMap<String,Object>();
    private final ScriptEngineManager scriptEngineManager;
    private AttachmentScript attachmentScript;
    private final ScriptEngine engine;

    public Scripter(){
        this.scriptEngineManager=new ScriptEngineManager();
        this.engine=scriptEngineManager.getEngineByName("JavaScript");
    }

    public String run(Reader reader) throws ScriptException {
        StringWriter stringWriter= new StringWriter();
        engine.put("http", new HttpScript(credentialList));
        HtmlScript htmlScript=new HtmlScript();
        engine.put("html", htmlScript);
        engine.put("xml", new XmlScript());
        engine.put("out", new WriteScript(stringWriter,htmlScript));
        engine.put("context", new ContextScript(this));
        engine.put("attachments", attachmentScript);
        ScriptContext scriptContext=engine.getContext();
        scriptContext.setWriter(stringWriter);
        engine.eval(reader);
        return stringWriter.toString();
    }


    public String run(String inlineScript) throws ScriptException {
        StringReader reader=new StringReader(inlineScript);
        return run(reader);
    }

    public void setCredentialList(List<Credential> credentialList) {
        this.credentialList = credentialList;
    }

    public void setAttachmentScript(AttachmentScript attachmentScript) {
        this.attachmentScript = attachmentScript;
    }
}
