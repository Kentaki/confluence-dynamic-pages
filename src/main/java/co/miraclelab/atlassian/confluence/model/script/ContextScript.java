package co.miraclelab.atlassian.confluence.model.script;

import co.miraclelab.atlassian.confluence.model.Scripter;

public class ContextScript {
    public final Scripter scripter;
    public ContextScript(Scripter scripter){
        this.scripter=scripter;
    }

    public void set(String key, Object object){
        scripter.objectMap.put(key,object);
    }

    public Object get(String key){
        return scripter.objectMap.get(key);
    }

}
