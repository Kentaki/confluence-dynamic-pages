AJS.$(document).ready(function(){
    console.log("I'm ready"); //TODO remove
    AJS.$(document).on("click","li.macro-list-item",function () {
        console.log("clicked!!1"); //TODO remove

        var thatId=AJS.$(this).attr("id");
        console.log(thatId); //TODO remove

        if(thatId.indexOf("javascript-file-runner")!=-1 || thatId.indexOf("javascript-line-runner")!=-1)
            checkLicence(thatId);
        
    })
});

function checkLicence(id) {
    var url = AJS.contextPath()+"/plugins/servlet/dynamicPages/configure?licenseCheck=true";
    AJS.$.ajax({
        url: url,
        type: 'GET',
        success: function(status) {
            if(status.match("^error")) {
                throwError("License Issue!","Functionality of "+id+" may has been limited!","auto");
            }
        },
        error: function(status){
            console.log(status);
        }
    });
}

function throwError(title,body,close){
    body="<p>"+body+"</p>";
    require(['aui/flag'], function(flag) {
        var myFlag = flag({
            type: 'error',
            title: title,
            close: close,
            persistent: false,
            body:   body
        });
    });
}