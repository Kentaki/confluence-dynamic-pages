var formDisabled=false;
AJS.$(document).ready(function () {
    AJS.$("#select-access").auiSelect2();

    AJS.$(document).on("click","input#form-submit-button",function(e){
        e.preventDefault();
        if(formDisabled)
            return false;
        var form=AJS.$(this).parents("form");
        makePost(form);
    });

    AJS.$(document).on("click","button#credential-delete",function(e){
        e.preventDefault();
        var id=AJS.$(this).parents("tr").attr("id");
        var confirmationMessage="Are you sure about deleting "+id+"?";
        var confirmation=confirm(confirmationMessage);
        if(confirmation) deleteMe(id);

    });

    AJS.$(document).on("click","button#credential-toggle",function(e){
        e.preventDefault();
        var id=AJS.$(this).parents("tr").attr("id");
        toggleMe(id);
    });
});



function throwError(title,body,close){
    body="<p>"+body+"</p>";
    require(['aui/flag'], function(flag) {
        var myFlag = flag({
            type: 'error',
            title: title,
            close: close,
            persistent: false,
            body:   body
        });
    });
}
function throwSuccess(title,body){
    body="<p>"+body+"</p>";
    require(['aui/flag'], function(flag) {
        var myFlag = flag({
            type: 'success',
            title: title,
            close: 'auto',
            persistent: false,
            body:   body
        });
    });
}

function disableForm(form){
    console.log("form disabled");
    AJS.$(form).find("input").prop("disabled", true);
    AJS.$(form).find("button").prop("disabled", true);
    formDisabled=true;
}
function enableForm(form){
    console.log("form enabled");
    AJS.$(form).find("input").prop("disabled", false);
    AJS.$(form).find("button").prop("disabled", false);
    AJS.$(form).find("input.decorator").prop("disabled", true);
    formDisabled=false;
}
function makePost(form){
    var data = form.serialize();
    disableForm(form);
    var url = $(this).attr("action");
    if (typeof attr === typeof undefined || attr === false)
        url="";

    AJS.$.post(url, data,
        function(status){
            console.log(status);//TODO remove
            if(status.match("^success")){
                form.trigger("reset");
                throwSuccess("Success","Credential has been added!");
                enableForm(form);
                window.setTimeout(function(){location.reload()},1000);
            }
            else if(status.match("^error")) {
                throwError("Error!",status,"auto");
                enableForm(form);
            }
        });
}

function deleteMe(id) {
    AJS.$.ajax({
        url: '?id='+id,
        type: 'DELETE',
        success: function(status) {
            if(status.match("^success")){
                var selector="table#credential-table tr#"+id;
                AJS.$(selector).fadeOut(50);
            }
            else if(status.match("^error")) {
                throwError("Error!",status,"auto");
            }
        },
        error: function(status){
            console.log(status);
        }
    });
}

function toggleMe(id) {
    AJS.$.ajax({
        url: '?id='+id,
        type: 'PUT',
        success: function(status) {
            if(status.match("^success")){
                var td="table#credential-table tr#"+id+" td.toggle";
                var span=AJS.$(td).find("span");
                if(span.html()==="Active")
                    AJS.$(td).html('<span class="aui-lozenge aui-lozenge-subtle aui-lozenge-success">Inactive</span>');
                else AJS.$(td).html('<span class="aui-lozenge aui-lozenge-success">Active</span>');
            }
            else if(status.match("^error")) {
                throwError("Error!",status,"auto");
            }
        },
        error: function(status){
            console.log(status);
        }
    });
}