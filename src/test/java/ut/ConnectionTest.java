package ut;

import co.miraclelab.atlassian.confluence.model.Connection;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by kentaki on 9/17/17.
 */
public class ConnectionTest {
    private Connection connection;
    @Before
    public void setup() throws Exception {
        connection=new Connection();

    }
//    @Test
    public void doGetWithoutCredential() throws Exception {
        connection.setUrl("http://localhost:8090/rest/api/space");
        connection.doGet();
        System.out.println(connection.getResponseBody());
    }
//    @Test
    public void doGetWithCredentials() throws Exception {
        connection.setUrl("http://localhost:8090/rest/api/space");
        connection.setUsername("admin");
        connection.setPassword("qwe123");
        connection.doGet();
        System.out.println(connection.getResponseBody());
    }

}